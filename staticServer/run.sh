projectPath=${GOPATH}/src/gitlab.com/extradiable/samples/staticServer

go build -o server ${projectPath}/main/main.go

if [[ $? -eq 0 ]]; then
  sudo setcap CAP_NET_BIND_SERVICE=+eip ${projectPath}/server
  ./server
fi


