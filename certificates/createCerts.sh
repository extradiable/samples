#!/bin/bash

openssl req -x509 -newkey rsa:4096 -keyout rootCA.key -out rootCA.pem -days 365 -nodes -config rootCA.cfg

openssl req -new -newkey rsa:2048 -keyout cert.key -out cert.csr -nodes -config cert.cfg
openssl x509 -req -in cert.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out cert.pem -days 365 -extensions v3_req 

touch index.txt
openssl ca -policy policy_anything -out cert.pem -config rootCA.cfg -extensions v3_req -in cert.csr -create_serial

#openssl req -new -x509 -newkey rsa:2048 -nodes -keyout cert.key -days 365 -out cert.pem -config cert.cfg 
cat cert.pem rootCA.pem > certChain.pem

openssl pkcs12 -export -in certChain.pem -inkey cert.key -name server -out boundle.p12 -passout pass:123

keytool -importkeystore -deststorepass 123456 -destkeystore test.keystore -destkeypass 123456 -deststoretype PKCS12 -srckeystore boundle.p12 -srcstoretype PKCS12 -srcstorepass 123

keytool -list -v -keystore test.keystore -storepass 123456
