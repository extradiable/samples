#!/bin/bash

projectPath=${GOPATH}/src/gitlab.com/extradiable/samples/submitFormServerSide

export CERTIFICATE=${projectPath}/certificates/certChain.pem
export KEY_FILE=${projectPath}/certificates/cert.key

echo $CERTIFICATE
echo $KEY_FILE

go build -o server ${projectPath}/main/main.go

if [[ $? -eq 0 ]]; then
  sudo setcap CAP_NET_BIND_SERVICE=+eip ${projectPath}/server
  ./server
fi


