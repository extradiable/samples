package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

const (
	DEFAULT_PORT = 443
)

var (
	router = mux.NewRouter()
)

func register(w http.ResponseWriter, r *http.Request) {

	log.Debug("Request has been received.")

	decoder := json.NewDecoder(r.Body)
	jsonMap := make(map[string](interface{}))
	err := decoder.Decode(&jsonMap)

	if err != nil {
		log.Errorf("Unexpected error when reading the json request: %v.", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, `{"operation": "failed"}`)
		return
	}

	encoder := json.NewEncoder(os.Stdout)
	err = encoder.Encode(jsonMap)
	if err != nil {
		log.Warningf("Unexpected error to encode json: %v.", err)
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprint(w, `{"operation": "completed"}`)

	log.Debug("Request has been processed.")
}

func main() {

	logFormat := os.Getenv("LOG_FORMAT")
	if len(logFormat) == 0 {
		logFormat = "json"
	}

	if logFormat == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{})
	}

	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)
	log.SetReportCaller(false)

	log.Info("Starting Server")

	serverPort := DEFAULT_PORT
	if len(os.Getenv("SERVER_PORT")) != 0 {
		tmp, err := strconv.Atoi(os.Getenv("SERVER_PORT"))
		if err == nil {
			serverPort = tmp
		}
	}
	log.Infof("Server Port: %d", serverPort)

	certFile := os.Getenv("CERTIFICATE")
	if len(certFile) == 0 {
		log.Fatal("No env var: CERTIFICATE")
	}
	log.Infof("Cert: %s", certFile)

	keyFile := os.Getenv("KEY_FILE")
	if len(keyFile) == 0 {
		log.Fatal("No env var: KEY_FILE")
	}
	log.Infof("key: %s", keyFile)

	router.SkipClean(true)
	router.Methods(http.MethodPost).Path("/register").HandlerFunc(register)

	tlsConfig := &tls.Config{}
	tlsConfig.ClientAuth = tls.NoClientCert
	tlsConfig.NextProtos = []string{"http/1.1"}

	srv := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf("0.0.0.0:%d", serverPort),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		TLSConfig:    tlsConfig,
	}

	log.Fatal(srv.ListenAndServeTLS(certFile, keyFile))
	log.Fatal(srv.ListenAndServe())
}
